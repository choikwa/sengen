import re
import sys

if len(sys.argv) < 2:
  print("need more than 1 arg. supply input texts")
  exit(1)

words = []
for textfile in sys.argv[1:]:
  with open(textfile, 'r') as f:
    lines = f.readlines()
    for line in lines:
      splitted = re.split(" |:|,|\.|\!|\?|=|\+|\[|\]|\(|\)|\{|\}|\0|\t|\n", 
                      line)
      #print("line: " + line)
      #print("splitted:" + str(splitted))
      for word in splitted:
        if word:
          words.append(word.lower())

def build_followers(words):
  followers = {}   # {word:{next:count}}
  for word in words:
    #print("word: {0}".format(word))
    for i,first in enumerate(words):
      if first == word and i < len(words)-1:
        if not followers.has_key(word):
          followers[word] = {}
        follower = followers[word]
        next_word = words[i+1]
        if follower.has_key(next_word):
          follower[next_word] += 1
        else:
          follower[next_word] = 1
  return followers

followers = build_followers(words)
#for k,v in followers.iteritems():
#  print("{0}:{1}".format(k, v))

import random
def weighted_choice(choices):
  total = sum(w for c, w in choices.iteritems())
  r = random.uniform(0, total)
  upto = 0
  for c, w, in choices.iteritems():
    if upto + w >= r:
      return c
    upto += w
  assert False, "Shouldn't get here"

sen = '' + words[0]
pinned = words[0]
for i in range(30):
  if followers.has_key(pinned):
    old = pinned
    pinned = weighted_choice(followers[pinned])
    followers[old][pinned] -= 1
  else:
    pinned = words[i % len(words)]
  sen += ' ' + pinned

print(sen)

